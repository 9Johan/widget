import 'package:flutter/material.dart';
import 'package:widget/widgets/timetable.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:riverpod/riverpod.dart';

void main() => runApp(ProviderScope(child: TimeTable()));
 