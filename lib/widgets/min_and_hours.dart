import 'dart:async';

import 'package:riverpod/riverpod.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class TimerState{
  final hours;
  final mins;
  TimerState(this.hours, this.mins);
}

class MinNotifier extends StateNotifier<TimerState>{
  late final Timer timer;
  final int oneMin = 1;
  int hour = 0;
  int min = 0;

  MinNotifier(): super(TimerState(1, 2)) {
    timer = Timer.periodic(Duration(seconds: oneMin), (timer) => decrement());
  }
  void decrement () {
    hour = state.hours;
    min = state.mins;
    if (min == 0 && hour == 0) {
      dispose();
      hour--;
      min = 5;
      state = TimerState(hour, min);
    } else if (min == 0) {
      hour--;
      min = 5;
      state = TimerState(hour, min);
    } else {
      min--;
      state = TimerState(hour, min);
    }
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }
}
