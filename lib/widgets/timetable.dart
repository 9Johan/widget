import 'package:flutter/material.dart';
import 'package:riverpod/riverpod.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:widget/widgets/min_and_hours.dart';



final timerProvider = StateNotifierProvider((ref) => MinNotifier());



class TimeTable extends ConsumerWidget {
  const TimeTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    
    final timer = watch(timerProvider);

    return MaterialApp(
      home: Center(
        child:Container(
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
          ),
          height: 93,
          width: 320,
          child: Column(
            children: [
              Container (
                height: 20,
                width: 320,
                decoration: BoxDecoration(
                  color: Color.fromRGBO(23, 109, 234, 1),
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(10),
                    topLeft: Radius.circular(10),
                  ),
                ),
                alignment: Alignment.center, 
                child : 
                  Text(
                    'через ${timer.hours}ч ${timer.mins}мин',
                    style: 
                      TextStyle(
                        decoration: TextDecoration.none,
                         fontSize: 12,
                          color: Colors.white,
                           fontFamily: 'Roboto-Regular',
                            fontWeight: FontWeight.w500,
                      ),
                  ),
              ),
              Padding(padding: EdgeInsets.only(bottom: 3)),
              Expanded(child: 
              Row(
                children: [
                  Padding(padding: EdgeInsets.only(left:11)),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Icon(Icons.access_time_rounded,size: 14, color: Color.fromRGBO(189, 190, 197, 1),),
                      Padding(padding: EdgeInsets.only(bottom: 5)),
                      Icon(Icons.location_on_outlined, size: 14, color: Color.fromRGBO(189, 190, 197, 1),),
                    ],
                  ),
                  Padding(padding: EdgeInsets.only(left: 3)), 
                  Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children : [
                        Text(
                          '10:20\n12:20',
                          style: TextStyle(
                            decoration: TextDecoration.none,
                            color: Color.fromRGBO(23, 109, 234, 1),
                            fontSize: 12, fontFamily: 'Roboto-Regular',
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                        Text(
                          '1111',
                          style: TextStyle(
                            decoration: TextDecoration.none,
                            color: Color.fromRGBO(23, 109, 234, 1),
                            fontSize: 12, fontFamily: 'Roboto-Regular',
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ],
                    ),
                  Padding(padding: EdgeInsets.only(left: 8)),
                  Container(
                    height: 57,
                    width: 1,
                    color: Color.fromRGBO(10, 132, 255, 1),
                  ),
                  Padding(padding: EdgeInsets.only(left: 8)),Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                            Text(
                              'Математический анализ',
                              style: 
                                TextStyle(
                                  decoration: TextDecoration.none, 
                                  fontSize: 15, color: Colors.black, 
                                  fontFamily: 'Roboto-Regular', 
                                  fontWeight: FontWeight.w500,
                                )
                            ),
                            Text(
                              'Бахтиярова Ольга Сергеевна', 
                              style: 
                                TextStyle(decoration: TextDecoration.none,
                                 fontSize: 12,
                                  color: Color.fromRGBO(23, 109, 234, 1),
                                   fontFamily: 'Roboto-Regular',
                                    fontWeight: FontWeight.w500,
                                )
                            ),
                        ],
                      ),
                ],
              )),
            ],
          ),
        ),
      ),
    );
  }
}